export const environment = {
    "envId": "bab95acd-ea25-296f-6860-9c4a652efdf4",
    "name": "dev",
    "properties": {
        "production": false,
        "baseUrl": "http://10.10.10.97:3000/bhive-art/",
        "tenantName": "neutrinos-templates-testing",
        "appName": "image-carousel",
        "namespace": "carousel app",
        "isNotificationEnabled": true,
        "googleMapKey": "AIzaSyCSTnVwijjv0CFRA4MEeS-H6PAQc87LEoU",
        "firebaseSenderId": "FIREBASE_SENDER_ID",
        "firebaseAuthKey": "FIREBASE_AUTH_KEY",
        "authDomain": "FIREBASE_AUTH_DOMAIN",
        "databaseURL": "FIREBASE_DATABASE_URL",
        "storageBucket": "FIREBASE_STORAGE_BUCKET",
        "messagingSenderId": "FIREBASE_SENDER_ID",
        "appDataSource": "new-rt",
        "appAuthenticationStrategy": "localAuth",
        "basicAuthUser": "username",
        "basicAuthPassword": "password",
        "authDataSource": "new-rt",
        "userDefaultExceptionUI": true
    }
}