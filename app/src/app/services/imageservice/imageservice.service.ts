/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';

@Injectable()
export class imageserviceService {
    images = [
        {
            imgSrc: "Web/photo2.jpg",
            imgPrice: "$ 1,105,000",
            imgDesc: "21 St, San Francisco",
            imgHeader: "Integer maximus"
        },
        {
            imgSrc: "Web/photo3.jpg",
            imgPrice: "$ 1,125,000",
            imgDesc: "Beverly, CA 90210",
            imgHeader: "Duis bibendum"

        },
        {
            imgSrc: "Web/photo4.jpg",
            imgPrice: "$ 1,200,000",
            imgDesc: "132, San Francisco",
            imgHeader: "Fusce quis"
        },
        {
            imgSrc: "Web/photo5.jpg",
            imgPrice: "$ 910,000",
            imgDesc: "132 St,London",
            imgHeader: "Quis libero"
        },
        {
            imgSrc: "Web/photo6.jpg",
            imgPrice: "$ 2,524,000",
            imgDesc: "Lockslee, San Francisco",
            imgHeader: "Pellentesque habitant"
        },
        {
            imgSrc: "Web/photo7.jpg",
            imgPrice: "$ 5,185,000",
            imgDesc: "San Francisco,CA 900212",
            imgHeader: "Maecenas sed purus"
        },
        {
            imgSrc: "Web/photo8.jpg",
            imgPrice: "$ 1,185,000",
            imgDesc: "Broadway,San Fransisco",
            imgHeader: "Nullam libero"
        }
    ];
    sidenavList = ["Home", "Dashboard", "Contact"]
    getImages() {
        return this.images;
    }
    getHomeList() {
        return this.sidenavList;
    }

}
